<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_lasalle' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g{?#H;,<KM{ E[B#WoVAiF#<z51$}=$<]t3*oJ9Wf%VeZRYKRjLuk~npaDlXsc06' );
define( 'SECURE_AUTH_KEY',  ')j3F66B/srRKF|wikhvDEhBBY/9UPM7,DST8q,G~=w7RGJy31MbnD$)zpk]TR#5V' );
define( 'LOGGED_IN_KEY',    'htiH;ttrL/{]s-m`l_i&;0KiXiVkOX}`Sb%(uBi48xueo)767?):VkZ[M10n?2^[' );
define( 'NONCE_KEY',        'txIaNu/TE?uLY1g?NC{p%=/JjpERc(qj4U$Ik=pGi2%KOLu{/~ye)xLAwp_jL@#j' );
define( 'AUTH_SALT',        'V< sRtHs+-/kKy6?j,tu;$AH0^ &wvny0vF~-M5BOs2$rj5S7(~@oL8pVuTa>(({' );
define( 'SECURE_AUTH_SALT', 'D#h;+<ulmG^C{Q(2 [;X4.>^;LfCwG4,h%tr9qTpr/=#`shm9>*0i!VQjTn/`-Th' );
define( 'LOGGED_IN_SALT',   'OS.6<DGQ^qso!?s](_K#s=5;pgbE0!!H/3GCK_1PsfnTx;E,%QEF#M!hy2,]q`Jr' );
define( 'NONCE_SALT',       'Y<):Jib *G24osMLm!moG#;D_a-qWaq|e$7(lqLS.lMoXl?afdbVIT.sL63nAK?S' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
