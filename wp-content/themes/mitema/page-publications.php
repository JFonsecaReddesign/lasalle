<?php get_header(); ?>
<div class="contain-fluid banner-blue-text " >
    <div class="filter-blue">
        <div class="convenios-banner text-on-banner ">
            <?php the_title('<h1>','</h1>') ?>
        </div>
    </div>
    <div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
    </div>
</div>
<div class=" all-publish">
    <div class="row">
        <div class="table-responsive">
        <table class="table table-striped table-borderless">
        <?php
            $categories = get_categories( array(
                'orderby' => 'term_id',
                'order'   => 'ASC',
            ));
                
         foreach( $categories as $category ) {
              
                ?>
            <tr>   
                <td >
                    <div class="col-lg-12 col-md-12 col-12 conten-circulares " data-aos="zoom-out-right">
                        <div class="row heigth-publications justify-content-md-center align-items-center">
                            <div class="col-12 col-md-4 circular-logo d-flex align-items-center  aling-items-category ">
                                <?php $image=get_field('imagen_cat',$category); ?>
                                <?php if (get_field('imagen_cat',$category) ): ?>
                                    <img class="img-fluid img-categories" src="<?php echo $image['url']; ?>" alt="<?php echo $category->name; ?>" > 
                                <?php else: ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/lasalle_fondo.png" class="img-categories img-fluid"/>
                                <?php endif ?>
                            </div>
                            <div class="col-12 col-md-8 circular-description">
                                <div>
                                    <h3 class="subtitle-category"><?php echo $category->name?></h3> 
                                    <a class="btn btn-category" href="<?php echo get_category_link($category->term_id)?>">Read more </a> 
                                </div>
                              
                            </div>
                            
                               
                            
                        </div>
                    </div>
                </td>
            </tr>    
        <?php } ?>
        
        </table> 
        </div>
    </div>
</div>
<br>
<br>
<?php get_footer(); ?>