<?php get_header(); ?>

<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue ">
	 	<div class="text-on-banner ">
	 		<?php the_title('<h1>','</h1>') ?>
	 		
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>


<div class="contain-fluid justify-content-center contain-fluid-finish">
	
		<div class="col-12 contain-map2 fluid-map" style="position: relative;">
			<div class="contact-form2 justify-content-center">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		                  <?php the_content(); ?>
		                <?php endwhile; endif; ?>
			</div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3976.6187437176072!2d-74.08014857406613!3d4.6618776944385!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2s!4v1417733316518"  frameborder="0" class="iframe-map"></iframe>


			
		</div>
	
	
</div>



<?php get_footer(); ?>				