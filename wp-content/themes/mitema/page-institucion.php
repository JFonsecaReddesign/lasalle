<?php get_header(); ?>

<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue ">
	 	<div class="text-on-banner">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 			<h2><?php the_field('sub-titulo'); ?></h2>
                    <p><?php the_content(); ?></p>
                <?php endwhile; endif; ?>
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>

<div class="contain-fluid row-stuck ">

	
	<?php while(have_posts()): the_post();?>
	<div class="col-12 institucion-contain">				
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-12 order-lg-1 order-2 info">
				<h2>Misión</h2>
				
				<div class="pre-scroll" >
					<p>  <?php the_field('mision'); ?> </p>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 order-lg-2 order-1 img-cont"  data-aos="flip-right">
				<?php
					$imagen = get_field('imagen_mision');
				?>
				<img src="<?php echo $imagen; ?>" class="imagen-caja img-fluid">
			</div>
		</div>
	</div>
	
	<div class="col-12 institucion-contain">				
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-12 order-lg-2 order-2 info">
				<h2>visión</h2>
				<div class="separadores">
					
					
				</div>
				<div class="pre-scroll" >
					<p>  <?php the_field('vision'); ?> </p>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 order-lg-1 order-1 img-cont"  data-aos="flip-right">
				<?php
					$imagen = get_field('imagen_vision');
				?>
				<img src="<?php echo $imagen; ?>" class="imagen-caja img-fluid">
			</div>
		</div>
	</div>

	<div class="col-12 institucion-contain">				
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-12 order-lg-1 order-2 info">
				<h2>Política</h2>
				<div class="separadores">
					
					
				</div>
				<div class="pre-scroll" >
					<p>  <?php the_field('politicas'); ?> </p>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 order-lg-2 order-1 img-cont"  data-aos="flip-right">
				<?php
					$imagen = get_field('imagen_politicas');
				?>
				<img src="<?php echo $imagen; ?>" class="imagen-caja img-fluid">
			</div>
		</div>
	</div>

	<div class="col-12 institucion-contain">				
		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-12 order-lg-2 order-2 info">
				<h2>Historia</h2>
				<div class="separadores">
					
					
				</div>
				<div class="pre-scroll" >
					<p>  <?php the_field('historia'); ?> </p>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 order-lg-1 order-1 img-cont"  data-aos="flip-right">
				<?php
					$imagen = get_field('imagen_historia');
				?>
				<img src="<?php echo $imagen; ?>" class="imagen-caja img-fluid">
			</div>
		</div>
	</div>


	<?php endwhile ?>

</div>


<?php get_footer(); ?>				