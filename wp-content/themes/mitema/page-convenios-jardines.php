<?php get_header(); ?>
<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue">
	 	<div class="convenios-banner text-on-banner ">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<p> <?php the_field('slogan'); ?></p>
	 		
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>
<div class="container-fluid  ">
	<div class="row row-convenios row-stuck">
		
	
		<div class="col-12  text-two-columns no-slide">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
                    <p><?php the_content(); ?></p>
                   
                    
            <?php endwhile; endif; ?>

		</div>
		<div class="divider">
    
        <hr>
    
    </div>
	</div>

	
</div>

<?php while(have_posts()): the_post();?>


<div class="container-fluid notes-admisiones amarillofeo">
	<div class="row row-stuck  justify-content-center">
		<div class="col-12 jardines-title">
			<h2><?php echo pll_e('convenio_jardines'); ?></h2>
		</div>
		<div class="col-12 col-lg-6   order-2 order-lg-1 text-two-columns no-slide-j" data-aos="zoom-in-right">
			<?php the_field('jardines'); ?>
		</div>
		<div class="col-12 col-lg-4 order-1 order-lg-2 convenios-j-slide">
			<?php
			if (strpos($post->post_content,'[gallery') === false){ ?>
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid">
		<?php }
			else{
				?>
				<div id="siple_carousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    
  
 
				<?php
  				$gallery = get_post_gallery( get_the_ID(), false ) ;
			 	foreach ( $gallery['src'] AS $id => $src ) { 
			 		if ($id==0) {
			 			?>
			 			<div class="carousel-item active">
			 			<?php
					}
					else
					{
						?>
			 			<div class="carousel-item">
			 			<?php
					}
			 		?>
				      <img class="d-block w-100" src="<?php echo "$src" ?>" >
				    
				    </div>
				    <?php
	          	} ?>
</div>
	       
	         <a class="carousel-control-prev" href="#siple_carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#siple_carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
	        <?php

	        }
		   	
		?>
		</div>
	</div>
</div>



	<?php endwhile ?>
<?php get_footer(); ?>