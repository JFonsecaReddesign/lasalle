<?php get_header(); ?>
	<?php
			$category = get_category_by_slug('lhemis-year-books');
	?>
<div  id="detail" class="container all-publish">
	<div class="title-category">
		<h2 class="other-title text-uppercase"><?php echo $category->name?></h2>
	</div>
	<div class="publication-name">
		<p class="publication-name">
			<?php
				echo $image = get_field('titulo_flipbook',$category);
			?>
		</p>
	</div>	
	<div class="container-flipbook">
		<?php
			echo $image = get_field('contentcat',$category);
		?>
		
	</div>
	<div>
		<h2 class="other-title">Other publications</h2>
	</div>
	<div class="publication-content row">
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		    $args = array(
            'post_type' => 'publicacion',
            'category_name' => 'lhemis-year-books',
			'posts_per_page' => 6,
			'orderby' => 'post_date',
		    'paged' => $paged,
			
			);
		$loop = new WP_Query( $args );
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		
		<?php endwhile; wp_reset_postdata(); ?>


		<?php 
		while($query->have_posts()): $query->the_post();?>
		<div class="col-lg-12 col-md-12 col-12 conten-circulares " data-aos="zoom-out-right">
			<div class="row heigth-publications justify-content-md-center align-items-center">
				<div class="col-12 col-md-2 circular-logo d-flex align-items-center justify-content-start">
				<?php $image = get_field('imagen_cat',$category);
				
				?>
					<figure class="container-image-publicaciones">
						<img src="<?php echo get_template_directory_uri(); ?>/images/pdf_icon.svg" class="icon-image img-fluid"/>
					</figure>
				</div>
				<div class="col-12 col-md-10 circular-description">
					<div class="container-content">
						<h3 class="subtitle-category"><?php the_title() ?></h3> 
						<div class="container-buttons">
							<?php the_content(); ?>
						<?php
							$file = get_field('link_descarga');
							if( $file ):
						?>
							<p>
								<a class="btn btn-category" href="<?php echo $file['url']; ?>" download class="download-file">download</a>
							</p>
						<?php endif; ?>	
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>
		<div class="col-12 paginate pg-noticias justify-content-between">
		<?php pagination_nocitias( $loop ); ?>
		</div>
	</div>
</div>
<br>
<br>
<?php get_footer(); ?>