<?php get_header(); ?>
<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue ">
	 	<div class="text-on-banner admisiones-header">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<p class="slogan-pages"> <?php the_field('slogan'); ?></p>
	 		
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>




<div class="contain-fluid contain-page">
	<div class="row">
		<div class="col-12 cuerpo">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<p><?php the_content(); ?></p>
			<?php endwhile; endif; ?>
			
		</div>
	</div>
</div>	
<div class="page-footer">
<?php get_footer(); ?>
</div>