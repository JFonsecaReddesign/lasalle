<?php get_header(); ?>

<div id="carouselBig" class="carousel slide  carousel-index  " data-ride="carousel">
  <div class="carousel-inner">
  	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 			
        <?php $url=apply_filters('the_content', $content); ?>
    <?php endwhile; endif; ?>
  	<?php


  		
		$gallery = get_post_gallery( $url, false );
		$ids = explode( ",", $gallery['ids'] );

		foreach( $ids as $id => $item ) {

		   $link   = wp_get_attachment_url( $item );

		   if ($id==0) {
		   	?>
		   		<div class="carousel-item active">
				    <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="First slide">
				</div>
		   	<?php
		   }
		   else{
		   	?>
		   		<div class="carousel-item">
			      <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="Second slide">
			    </div>
		   	<?php
		   	
		   }
		} 
	?>
  
  </div>
<a class="carousel-control-prev" href="#carouselBig" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselBig" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>
<div class="container ">
	<div class="col-12 escuelas contain-page">
		<?php the_title('<h1>','</h1>') ?>
	</div>
	
</div>

<div class="container-fluid escuelas-container">

	<div class="row row-stuck justify-content-center">
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				
				<?php
					$imagen = get_field('futsal');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >

				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('futsal'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">

				<?php
					$imagen = get_field('ultimate');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('ultimate'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('gimnasia');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('gimnasia'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('atletismo');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('atletismo'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('voleibol');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('voleibol'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('futbol');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('futbol'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('baloncesto');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('baloncesto'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('astronomia');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('astronomia'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('porras');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('porras'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('danzas');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('danzas'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('matematicas');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('matematicas'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('musica');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('musica'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('zumba');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('zumba'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('ingles');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('ingles'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('dibujo_manga');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('dibujio_manga'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('arts_lab');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('arts_lab'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('pintura');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('pintura'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('taekwondo');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('taekwondo'); ?></h3>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a class="hover-areas-link escuelas-item" >
			<div class="col-12">
				<div style="position: relative;">
				<?php
					$imagen = get_field('ballet');
				?>
		   		<img src="<?php echo $imagen['url']; ?>"  class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<h3><?php echo pll_e('ballet'); ?></h3>
			</div>
			</a>
		</div>
	</div>
</div>


<?php get_footer(); ?>