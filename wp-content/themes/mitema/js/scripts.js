$ = jQuery.noConflict();

$(function() {
  AOS.init();
});

$( document ).ready(function() {

    $('#ModalEntrada').modal({show:true});

    $( ".page-numbers.prev" ).addClass( "prev-b" );
    $( ".page-numbers.prev" ).removeClass( "prev" );

    $( ".page-numbers.next" ).addClass( "next-b" );
    $( ".page-numbers.next" ).removeClass( "next" );

  
});
$(".pastoral-function").click(function(){
  item=$(this).attr('value');
  $(".galery-independent").hide();
  $("#galery_independent_"+item).show();

  $('html, body').animate({scrollTop:$("#galery_independent_"+item).position().top-70}, 'fast');
  return false;
});

$(".closer-cross").click(function(){
  item=$(this).attr('value');
  
  $(".galery-independent").hide();
  $('html, body').animate({scrollTop:$("#pastoral-item_"+item).position().top-70},'slow' );
  return false;
});


$("#the_arrow_up").click(function(){
 
  $('html, body').animate({scrollTop:$("#principal-var").position().top-70},'slow' );
  return false;
});



$("#sub-menu-door").click(function(){

	if ($( "#container-sub-menu" ).hasClass( "sub-menu-open" )) {
    $("#sub-menu").hide();
    $('#container-sub-menu').removeClass('animated bounceIn');
		$( "#container-sub-menu" ).removeClass( "sub-menu-open" );
		$( "#container-sub-menu" ).addClass( "sub-menu-close" );
		
		
	} else if ($( "#container-sub-menu" ).hasClass( "sub-menu-close" )) {
		$("#sub-menu").show();
    $('#container-sub-menu').addClass('animated bounceIn');
		$( "#container-sub-menu" ).removeClass( "sub-menu-close" );
		$( "#container-sub-menu" ).addClass( "sub-menu-open" );
	}

	return false;

});


$( document ).ready(function() {
   $("#sub-menu").hide();
		$( "#container-sub-menu" ).removeClass( "sub-menu-open" );
		$( "#container-sub-menu" ).addClass( "sub-menu-close" );

	// manual carousel controls
    $('.next').click(function(){ $('#postsCarousel').carousel('next');return false; });
    $('.prev').click(function(){ $('#postsCarousel').carousel('prev');return false; });
});


$(document).ready(function() {
   $("#postsCarousel").swiperight(function() {
   	$(this).carousel('prev');
    });
   $("#postsCarousel").swipeleft(function() {
      $(this).carousel('next');
   });
});



$("#glass").click(function(){
  if ($( "#glass" ).hasClass( "s-open" )){
    $('#mysearch').addClass('animated pulse');
    $("#mysearch").hide();
    $( "#glass" ).addClass( "s-close" );
    $( "#glass" ).removeClass( "s-open" );
  }
  else{

    $('#mysearch').addClass('animated pulse');
    $("#mysearch").show();
    $( "#searching" ).focus();
    $( "#glass" ).removeClass( "s-close" );
    $( "#glass" ).addClass( "s-open" );
  }
  

});

$(document).mouseup(function(e) 
{
    var container = $("#mysearch");

    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.hide();
        var container = $("#glass");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
        $( "#glass" ).addClass( "s-close" );
        $( "#glass" ).removeClass( "s-open" );
      }
    }
});


$( "#searchform" ).submit(function( event ) {
  if ($("#searching").val().length > 3){
    return true;
    }
  else
  { 
    return false;
  }
  
});


$(".steps-click").click(function(){
  item=$(this).attr('value');
  $(".step-description").addClass( "d-md-none" );
  $("#step-"+item).removeClass( "d-md-none" );

  $(".step-logo-y").hide();
  $(".step-logo-b").show();
  $(".step-logo-b", this).hide();
  $(".step-logo-y", this).show();


  return false;
});



/* quitar caracteres */
$( document ).ready(function() {
  var t_o_s = $("a:lang(es-CO)").text()
  var t_o_e = $("a:lang(en-US)").text()

  if ($(window).width() > 1199) {
      var t = $("a:lang(es-CO)").text().substring(0, 2);
      $("a:lang(es-CO)").text(t);
      var t = $("a:lang(en-US)").text().substring(0, 2);
      $("a:lang(en-US)").text(t);
    }

  $( window ).resize(function() {
     if ($(window).width() > 1199) {
      var t = $("a:lang(es-CO)").text().substring(0, 2);
      $("a:lang(es-CO)").text(t);
      var t = $("a:lang(en-US)").text().substring(0, 2);
      $("a:lang(en-US)").text(t);
    }
    else{
       $("a:lang(es-CO)").text(t_o_s);
       $("a:lang(en-US)").text(t_o_e);
    }
  });

});
/* Quitar caracteres */

