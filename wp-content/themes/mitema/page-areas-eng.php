<?php get_header(); ?>
<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue">
	 	<div class="text-on-banner ">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <p><?php the_content(); ?></p>
                <?php endwhile; endif; ?>
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>
<div class="contain-fluid areas-contain" >
	<div class="col-12 row-stuck">
	<div class="row ">
		<?php
		    $args = array(
		    'post_type' => 'areas',
		    'posts_per_page' => -1,
			
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-6 col-xl-3 col-xs-12 area-conten" data-aos="zoom-out-down">
			<a href="<?php the_permalink() ?>" class="hover-areas-link" >
			<div class="col-12">
				<div style="position: relative;">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="areas-img" >
				<div class="hover-areas">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		 </div>
		   		</div>
			 
		    </div>

			<div class="col-12 areas-title" data-aos="fade-up" >
				<?php the_title('<h3>','</h3>') ?>
			</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>
	</div>
	</div>
</div>



<?php get_footer(); ?>
