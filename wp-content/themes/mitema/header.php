<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
	<?php wp_head();  ?>



    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


    <link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"
  integrity="sha384-OHBBOqpYHNsIqQy8hL1U+8OXf9hH6QRxi0+EODezv82DfnZoV7qoHAZDwMwEJvSw"
  crossorigin="anonymous">

  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<title></title>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>



    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.mobile.custom.min.js"></script>

</head>
<body <?php body_class() ?> id="body_id" >


<div class="sidenav" style="display: none;">
    <div  class="sidenav-div"  >
        <a href="#"><i class="fas fa-book"></i></a>
    </div>
        
    <div class="sidenav-div">
        <a href="#"><i class="fas fa-dollar-sign"></i>  </a>
    </div>
    <div class="sidenav-div">
        <a href="#"><i class="fas fa-utensils"></i> </a>
    </div>
    <div class="sidenav-div">
        <a href="#"><i class="fas fa-address-book"></i> </a>
    </div>
    <div class="sidenav-div">
        <a href="#"><i class="fas fa-user-friends"></i></a>
    </div>
</div>

<nav id="principal-var" class="navbar navbar-expand-xl  fixed-top">


<?php
  
    if(is_singular($post_type='areas') || is_search() || is_singular($post_type='noticias')|| is_singular($post_type='cronograma'))
    {
        echo"<div id='bar_top' class='bar_dark'>";
    }
    else
    {
         echo"<div id='bar_top'>";
    }
?>

    
    <?php
    	if (function_exists('the_custom_logo')) {
        	the_custom_logo();
        }
    ?>
    
    <div id="div-toogler" >
        <button class="navbar-toggler" id="target" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>    
    </div>
</div>

<?php
	$args = array(
    	'theme_location' => 'header-menu',
    	'menu_class' => 'nav navbar-nav', 
        'depth' => 2, // 1 = no dropdowns, 2 = with dropdowns.
    	'menu_id' => 'menuId', 
    	'container' => 'div',
        'container_class' => 'collapse navbar-collapse',
        'container_id' => 'navbarSupportedContent',
        'menu_class'      => 'navbar-nav mr-auto',
    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
    'walker'          => new WP_Bootstrap_Navwalker(),
        'wp_nav_menu_items' => 'tales'

        
    );
	wp_nav_menu($args);
  ?>
</nav>

<div id="container-sub-menu" class="sub-menu-open">
    <a href="#" id="sub-menu-door">
        <div class="title">

          <b class="link-title-services">
            <?php echo pll_e('servicios'); ?>
          </b>

        </div>
    </a>
    <?php
        $args = array(
            'theme_location' => 'sub-menu',
            'menu_class' => 'lateral-menu', 
            'menu_id' => 'sub-menu-id', 
            'container' => 'div',
            'container_class' => 'sub-menu',
            'container_id' => 'sub-menu'
        );
        wp_nav_menu($args);
      ?>
</div>


