<?php get_header(); ?>
<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue">
	 	<div class="text-on-banner ">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <p><?php the_content(); ?></p>
            <?php endwhile; endif; ?>
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>

<div class="container search-container">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<div class="row">
				
					<?php
						$tags="";
					 	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$args = array( 'post_type' => 'noticias', 'posts_per_page' => 2,
								'paged' => $paged, 
								);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-12 search-item per-notice">
					<div class="row">
						<div class="d-none d-sm-block col-sm-2">
							
						</div>
						<div class="col-12 col-sm-10">
						 <?php the_title('<h2>','</h2>') ?>
						</div>

						<div class="d-none d-sm-block col-sm-2 lateral-date">
							<div class="nc-date-noticiero float-right">
		                        <time>
		                          <div class="day">
		                          	<h3>
		                          	
		                          		<?php echo the_time('d'); ?><br>
		                          		<?php the_time('M'); ?>
		                          	
		                          	</h3>
		                          </div>
		                          <div class="month">
		                          	<b><?php the_time('Y'); ?></b>
		                          </div>
		                        </time>
		                    </div>
						</div>
						<div class="col-12 col-sm-10">



						
						 <div class="noticiero-card">
						 <div class="nc-date-inside -block d-sm-none">
		                        <time>
		                          <div class="day">
		                          	<h3>
		                          	<b>
		                          		<?php echo the_time('d'); ?><br>
		                          		<?php the_time('M'); ?>
		                          	</b>
		                          	</h3>
		                          </div>
		                          <div class="month">
		                          	<b><?php the_time('Y'); ?></b>
		                          </div>
		                        </time>
		                    </div>
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid">
						 </div>
								<p><?php the_excerpt(); ?></p>
								<a href="<?php the_permalink() ?>" class="btn">Leer Más</a>

										</div>
									</div>
																
								</div>
							<?php endwhile;?>
					<?php wp_reset_query(); ?>
				
				<div class="col-12 paginate pg-noticias justify-content-between">
						<?php pagination_nocitias( $loop ); ?>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-xs-12 info_single_one noticiero-tags ">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	      		<h3>Tags:</h3>
	      		
	      		<div class="container-tags">
	        		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
								
								<?php  
									$tags=the_tags('',''); ?>
								
							<?php endwhile;?>
	        	</div>
	     
	    <?php endwhile; endif; ?>
		</div>
	
		
	</div>
</div>








<?php get_footer(); ?>