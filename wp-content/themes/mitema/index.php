<?php get_header(); 
$currentlang = get_bloginfo('language');
?>


<div id="carouselBig" class="carousel slide  carousel-index  carousel-type-big" data-ride="carousel">
  <div class="carousel-inner">

  	<?php
  		$url = get_page_by_title('slide_banner');
		$gallery = get_post_gallery( $url, false );
		$ids = explode( ",", $gallery['ids'] );

		foreach( $ids as $id => $item ) {

		   $link   = wp_get_attachment_url( $item );

		   if ($id==0) {
		   	?>
		   		<div class="carousel-item active">
				    <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="First slide">
				</div>
		   	<?php
		   }
		   else{
		   	?>
		   		<div class="carousel-item">
			      <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="Second slide">
			    </div>
		   	<?php
		   	
		   }
		} 
	?>
  
  </div>
  

  <a class="carousel-control-prev" href="#carouselBig" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselBig" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>


<div id="carouselSmall" class="carousel slide  carousel-index carousel-type-small" data-ride="carousel">
  <div class="carousel-inner ">

  	<?php
  		$url = get_page_by_title('slide_banner_movil');
		$gallery = get_post_gallery( $url, false );
		$ids = explode( ",", $gallery['ids'] );

		foreach( $ids as $id => $item ) {

		   $link   = wp_get_attachment_url( $item );

		   if ($id==0) {
		   	?>
		   		<div class="carousel-item active">
				    <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="First slide">
				</div>
		   	<?php
		   }
		   else{
		   	?>
		   		<div class="carousel-item">
			      <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="Second slide">
			    </div>
		   	<?php
		   	
		   }
		} 

		

		
  	?>
  
  </div>
  

  <a class="carousel-control-prev" href="#carouselSmall" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselSmall" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  
</div>

<div class="container-fluid slogan-container" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/bq_quote.jpg') ">
	<div class="col-12" >
		
		<?php if($currentlang=="en-US") { ?>
			<h4 class="slogan-quotes"><?php echo esc_html(get_option('rds_eslogan_en')) ?>
			</h4>
		<?php } else{ ?> 
			<h4 class="slogan-quotes"><?php echo esc_html(get_option('rds_eslogan')) ?>
			</h4>
		<?php } ?>
	</div>
</div>

<div  class="container-fluid video-container">

<?php if($currentlang=="en-US") { ?>
			<iframe id="video-index" src="<?php echo esc_html(get_option('rds_video_en'))?>" frameborder="0" allow="autoplay; encrypted-media" ></iframe>

<?php } else{ ?> 
			<iframe id="video-index" src="<?php echo esc_html(get_option('rds_video'))?>" frameborder="0" allow="autoplay; encrypted-media" ></iframe>

<?php } ?>


</div>
<div class = "container mb-4">
	<div class="row">
		<div class = "title-events col-6">
			<h2>Eventos</h2>
		</div>
		<div class = "title-events text-right col-6">
			<?php
			$args = array(
				'post_type' =>'boton_cronograma',
				'posts_per_page' => 1,
			);
			$loop = new WP_Query($args);
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<?php
			$file = get_field('pdf');
			?>
				<a class="btn btn-primary" href="<?php echo $file['url']; ?>" target="_blank" rel="noopener noreferrer">CRONOGRAMA PDF</a> <?php echo the_content();?>
			<?php endwhile ?>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
			<div class="row">
				<?php
				$args = array( 
					'post_type' => 'evento',
					'posts_per_page' => 3,
					);
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-md-12 text-center con-cot-cro">
					<div class="card not-cro">
					  <div class="nc-date">
                        <time>
                          <div class="day">
                          	<h3><b><?php echo the_time('d'); ?></b></h3>
                          </div>
                          <div class="month">
                          	<h3><b><?php the_time('M'); ?></b></h3>
                          </div>
                        </time>
                      </div>
					  <?php the_post_thumbnail('evento') ?>
					  <div class="card-body">
					    <?php the_title('<h5>','</h5>'); ?>
					    <div class="card-text"><?php the_content(); ?></div>
					     <a href="<?php the_permalink() ?>" class="btn btn-blue"><?php echo pll_e('leer_mas'); ?></a>
					   
					  </div>
					</div>
				</div>
				<?php endwhile ?>
			</div>
	</div>
	</div>
	
</div>


<div class="container-fluid nc-container" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/news-crono.jpg')">

<div class="row row-nc row-stuck" >
	<div class="col-lg-12 col-md-12 n-cards" data-aos="fade-up"  data-aos-delay="250">
		<div class="col-xs-12 text-center nc-title">
			<h2><?php echo pll_e('noticias'); ?></h2>
			<h5><?php echo pll_e('actualidad'); ?></h5>
				
		</div>
		<div class="col-md-12">
			<div class="row">
			<?php
				$args = array( 
					'post_type' => 'noticias',
					'posts_per_page' => 3,
					);
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();?>
				<div class="col-md-3 text-center con-cot-cro">
					<div class="card not-cro">
					  <div class="nc-date">
                        <time>
                          <div class="day">
                          	<h3><b><?php echo the_time('d'); ?></b></h3>
                          </div>
                          <div class="month">
                          	<h3><b><?php the_time('M'); ?></b></h3>
                          </div>
                        </time>
                      </div>
					  <?php the_post_thumbnail('noticias') ?>
					  <div class="card-body">
					    <?php the_title('<h5>','</h5>'); ?>
					    <div class="card-text"><?php the_excerpt(); ?></div>
					     <a href="<?php the_permalink() ?>" class="btn btn-blue"><?php echo pll_e('leer_mas'); ?></a>
					   
					  </div>
					</div>
				</div>
				<?php endwhile ?>

				<div class="col-md-3 text-center con-cot-cro">
					<div class="card not-cro">
					  
					  <img src="<?php echo esc_html(get_option('artes_imagen')) ?>">
					  <div class="card-body">
					  
					  <h5><?php  echo pll_e('title_artes') ?></h5>
					    <div class="card-text"><?php echo pll_e('text_artes') ?></div>
					     <a href="<?php echo esc_html(get_option('artes_link')) ?>" class="btn btn-blue" target="_blank"><?php echo pll_e('boton_artes') ?></a>
					   
					  </div>
					</div>
				</div>

				<?php
					
   			
		    if($currentlang=="en-US"){
		    	$n= get_permalink( get_page_by_path( 'news' ) );
		    	
		    }
		    else{
		    	$n= get_permalink( get_page_by_path( 'noticiero' ) );
		    }
		

		

					$args = array( 'post_type' => 'noticias' );
					$t=pll_count_posts(pll_current_language(), $args);
					
					if ($t>3) { ?>
						<div class="col-12 text-center div-more">
						  <a href="<?php echo $n; ?>" class="btn"><?php echo pll_e('mas_eventos'); ?></a>
						</div>
				<?php }	?>
				
			
			</div>
		</div>
	</div>
	

</div>
</div>




<div class="container-fluid links-carousel-container">
<div class="row">
	<div class="col-12 ">
	<div id="linksCarousel" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">
		
		<?php
				if($currentlang=="en-US"){
		    		$esc= get_permalink( get_page_by_path( 'schools' ) );
		    		$cir= get_permalink( get_page_by_path( 'mailshot' ) );
		    		$inm= get_permalink( get_page_by_path( 'immersion-en' ) );
		    	
			    }
			    else{
			    	$esc= get_permalink( get_page_by_path( 'escuelas' ) );
			    	$cir= get_permalink( get_page_by_path( 'circulares' ) );
			    	$inm= get_permalink( get_page_by_path( 'inmersion' ) );
			    }
			?>


	    <div class="carousel-item active">
		    <a href="<?php echo $esc; ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/extracurriculares.svg"  class="img-link-carousel" >
		      	<h5 class="links-title"><?php echo pll_e('extracurriculares'); ?></h5>
		     </a>
		</div>

	    <div class="carousel-item ">
		    <a href="<?php echo $cir; ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/circulares.svg"  class="img-link-carousel" >
		      	<h5 class="links-title"><?php echo pll_e('circulares'); ?></h5>
		     </a>
		</div>
	    
	    <div class="carousel-item" >
	      <a href="<?php echo $inm ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/internacional.svg"  class="img-link-carousel" >
		      	<h5 class="links-title"><?php echo pll_e('intercambios'); ?></h5> 
		     </a>
	    </div>
	    
	  </div>
	<a class="carousel-control-prev" href="#linksCarousel" role="button" data-slide="prev">
		<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_l.svg" class="carousel-control-prev-icon" >
	    
	  </a>
	  <a class="carousel-control-next" href="#linksCarousel" role="button" data-slide="next">
	    <img src="<?php echo get_template_directory_uri(); ?>/images/arrow_r.svg" class="carousel-control-prev-icon" >
	  </a>
	</div>
	</div>
</div>
</div>

<div class="container-fluid links-normal-container" id="finkta">
	<div class="row">
		<div class="col-4 text-center">
			<?php
				if($currentlang=="en-US"){
		    		$esc= get_permalink( get_page_by_path( 'schools' ) );
		    		$cir= get_permalink( get_page_by_path( 'mailshot' ) );
		    		$inm= get_permalink( get_page_by_path( 'immersion-en' ) );
		    	
			    }
			    else{
			    	$esc= get_permalink( get_page_by_path( 'escuelas' ) );
			    	$cir= get_permalink( get_page_by_path( 'circulares' ) );
			    	$inm= get_permalink( get_page_by_path( 'inmersion' ) );
			    }
			?>
			<a href="<?php echo $esc; ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/extracurriculares.svg"  class="img-link-carousel" >
		      	<h5 class="links-title"><?php echo pll_e('extracurriculares'); ?></h5>
		     </a>	
		</div>
	   	<div class="col-4 text-center">
	   		<a href="<?php echo $cir; ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/circulares.svg"  class="img-link-carousel" >
		      	<h5 class="links-title"><?php echo pll_e('circulares'); ?></h5>
		    </a>
	    </div>
	    <div class="col-4 text-center">
	    	<a href="<?php echo $inm ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/internacional.svg"  class="img-link-carousel" >
		      	<h5 class="links-title"><?php echo pll_e('intercambios'); ?></h5> 
		    </a>
	    </div>
	</div>
</div>


<?php
 
$args = array(
   'posts_per_page' => 1 ,
   'category' => 'modal'
);
 
// Custom query.
$query = new WP_Query( $args );
 

   
while($query->have_posts()): $query->the_post();?>

	<?php 
		$x = get_field('activado');
		if($x == 1){ ?>
		
		<div class="modal fade" id="ModalEntrada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><?php the_title() ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-ini">
        <?php the_content() ?></h5>
      </div>
      
    </div>
  </div>
</div>
	
	<?php } ?>
		
 


<?php
	endwhile; wp_reset_postdata(); 
?>








<?php get_footer(); ?>

