<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-12 single-areas">
			<?php the_title('<h1 class="title-noticia">','</h1>') ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <p>
                   		<?php the_date(); ?>                    </p>
            <?php endwhile; endif; ?>
	 	</div>
	</div>
</div>




<div class="container pastoral-container">
	<div class="row">
		<div class="col-md-7">
			
				<div class="col-12">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid">
				</div>
				<div class="col-12">
					
		 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	                    <p><?php the_content(); ?></p>
	                <?php endwhile; endif; ?>
				</div>
		
		</div>

		<div class="col-md-3 info_single_one ">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	      		<h3>Tags:</h3>
	      		
	      		<div class="container-tags">
	        		<?php the_tags('',''); ?>
	        	</div>
	     
	    <?php endwhile; endif; ?>
			
		</div>
	</div>
</div>




<?php get_footer(); ?>