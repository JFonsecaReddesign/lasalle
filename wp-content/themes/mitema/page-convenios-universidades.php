<?php get_header(); ?>
<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue">
	 	<div class="convenios-banner text-on-banner ">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<p> <?php the_field('slogan'); ?></p>
	 		
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>
<div class="container-fluid ">
	<div class="row row-convenios row-stuck">
		
	
		<div class="col-12  text-two-columns">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
                    <p><?php the_field('texto'); ?></p>
                   
                    
            <?php endwhile; endif; ?>

		</div>
		<div class="divider">
    
        <hr>
    
    </div>
	</div>

	
</div>

<?php while(have_posts()): the_post();?>


<div class="container-fluid notes-admisiones">

	<div class="col-12 ">
	<div id="admisionesCarousel" class="carousel slide" data-ride="carousel">
	
	<ol class="carousel-indicators carousel-indicators-numbers">
		<?php echo pll_e('etapa'); ?>
    <li data-target="#admisionesCarousel" data-slide-to="0" class="active">1</li>
    <li data-target="#admisionesCarousel" data-slide-to="1">2</li>
    <li data-target="#admisionesCarousel" data-slide-to="2">3</li>
  </ol>

	<div class="carousel-inner">
	 
	 
    	
	  	<div class="carousel-item active ">
	  		<div class="text-two-columns">
		  		<h2><?php echo pll_e('etapa_uno'); ?></h2>
		  		<?php the_field('etapa_1'); ?>
	  		</div>
	  	</div>
	  	<div class="carousel-item ">
	  		<div class="text-two-columns">
	  			<h2><?php echo pll_e('etapa_dos'); ?></h2>
	  			<?php the_field('etapa_2'); ?>
	  		</div>
	  	</div>
	  	<div class="carousel-item ">
	  		<div class="text-two-columns">
	  			<h2><?php echo pll_e('etapa_tres'); ?></h2>
	  			<?php the_field('etapa_3'); ?>
	  		</div>
	  	</div>

	  	

	    
	    
	    
	  </div>
	<a class="carousel-control-prev notes-arrows" href="#admisionesCarousel" role="button" data-slide="prev">
		<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_l_b.svg" class="notes-arrows-img" >
	    
	  </a>
	  <a class="carousel-control-next notes-arrows" href="#admisionesCarousel" role="button" data-slide="next">
	    <img src="<?php echo get_template_directory_uri(); ?>/images/arrow_r_b.svg" class="notes-arrows-img" >
	  </a>
	</div>
	</div>

</div>

<div class="container-fluid contain-logos-u ">
	<div class="row row-stuck univ-logos justify-content-center">
		<div class="col-12  interinstitu">
			<h2><?php echo pll_e('universidades_sub'); ?></h2>
		</div>

		<?php
  		$url = get_page_by_title('convenios-universidades');
		$gallery = get_post_gallery( $url, false );
		$ids = explode( ",", $gallery['ids'] );

		foreach( $ids as $id => $item ) {
			$link   = wp_get_attachment_url( $item );

			?>
			<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2  align-items-center justify-content-center logos-universidades" data-aos="zoom-in" >
				<img class="img-fluid"  src="<?php echo $link ?>" >
			</div>
			
		<?php } ?>
			
		
	</div>
</div>

	<?php endwhile ?>
<?php get_footer(); ?>