  <footer>
	<div class="container-fluid footer-con " style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/bq_quote.jpg') ">
		<div class="row row-comunity">
			<div class="col-12 arrow_up">
            <a href="#" id="the_arrow_up">
                <img src="<?php echo get_template_directory_uri(); ?>/images/arrow_up.gif" >
            </a>
      </div>
			<div class="col-12 col-lg-2 comunity-logo">
				<img src="<?php echo get_template_directory_uri(); ?>/images/logo_lasalle_bogota.svg">
			</div>
			<div class="col-12 col-lg-8">
				<div class="row">
					<div class="col-12 col-md-4 text-center order-2 order-md-1">
						<a  target="_blank" class="links-footer" href="http://www.vatican.va ">www.vatican.va </a> 
						<br>
						<a  target="_blank" class="links-footer" href="http://www.relal.org.co">www.relal.org.co</a> 
						
					</div>
					<div class="col-12 col-md-4 text-center order-1 order-md-1">
						 Carrera 52 N° 64A - 99 <br>
                         Barrio San Miguel Bogotá D.C <br>
                         6302373 - 6308219
					</div>
					<div class="col-12 col-md-4 text-center order-3 order-md-1">
						<a  target="_blank" class="links-footer" href="https://www.lasalle.edu.co/">www.lasalle.edu.co</a>  
						<br>
						<a  target="_blank" class="links-footer" href="http://www.lasalle.org">www.lasalle.org</a>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-2 text-center c-icons">
				<a href="<?php echo esc_html(get_option('rds_facebook')) ?>"><i class="fab fa-facebook-f comunity-icon"></i></a>
                <a href="<?php echo esc_html(get_option('rds_tweeter')) ?>"><i class="fab fa-twitter comunity-icon"></i></a>
                <a href="<?php echo esc_html(get_option('rds_instagram')) ?>"><i class="fab fa-instagram comunity-icon"></i></a>
                <a href="<?php echo esc_html(get_option('rds_youtube')) ?>"><i class="fab fa-youtube comunity-icon"></i></a>
			</div>
			<div id="footer-comodin">
				<br>
			</div>
		</div>
	</div>

    
  </footer>
<div id="fixed-servicios">
  	
<?php
	$menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)
                                           // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

$menuID = $menuLocations['sub-menu']; // Get the *primary* menu ID

$primaryNav = wp_get_nav_menu_items($menuID); 

$count=count($primaryNav);

 ?>



<section class="carousel slide" data-interval="false" data-ride="carousel" data-pause="hover" id="postsCarousel">

 <div class="carousel-inner">
<?php foreach ( $primaryNav as $key=>$navItem ) { 
    $key = $key+1;


   ?>
    <?php if($key == 1) { ?>
        <div class="carousel-item active">
	<?php } ?>
    	
    <div class="col-3 sub-menu-footer">
      <?php 
      	$class = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $navItem->classes ), $navItem) ) );
      	$url=$navItem->url;
      	$title=$navItem->title;
        if ($class == 'sic') {
            echo " <a href='$url'><img src='".get_template_directory_uri()."/images/sic_b.svg' class='logos-servicios-footer'><b>$title</b></a>";
            }
        if ($class == 'plinea') {
            echo " <a href='$url'><img src='".get_template_directory_uri()."/images/pagoenlinea_b.svg' class='logos-servicios-footer' ><b>$title</b></a>";
            }
        if ($class == 'rest') {
            echo " <a href='$url'><img src='".get_template_directory_uri()."/images/restaurante_b.svg' class='logos-servicios-footer'><b>$title</b></a>";
            }
        if ($class == 'coe') {
            echo "<img src='".get_template_directory_uri()."/images/coeducacion_b.svg'  class='logos-servicios-footer'><b>$title</b></a>";
            }
        if ($class == 'egre') {
            echo " <a href='$url'><img src='".get_template_directory_uri()."/images/egresado_b.svg'  class='logos-servicios-footer'><b>$title</b></a>";
            }
        if ($class == 'pub') {
            echo " <a href='$url'><img src='".get_template_directory_uri()."/images/publicaciones_b.svg'  class='logos-servicios-footer'><b>$title</b></a>";
            }
        ?>
      </div>

    


    <?php if($key%4 == 0 || $key == $count) { ?>
        </div>
    <?php } ?>

    <?php if ($count > 4 && $key%4 == 0 ) { ?>
      	<div class="carousel-item">
    <?php } ?>


<?php } ?>
    
    

 </div>  
 <?php if ($count>4) { ?>
 	<a class="carousel-control-prev notes-arrows" href="#postsCarousel" role="button" data-slide="prev">
		<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_l_b.svg" class="notes-arrows-img" >
	    
	  </a>
	  <a class="carousel-control-next notes-arrows" href="#postsCarousel" role="button" data-slide="next">
	    <img src="<?php echo get_template_directory_uri(); ?>/images/arrow_r_b.svg" class="notes-arrows-img" >
	</a>
 <?php }  ?> 
  
</section>

</div>

<?php
		wp_footer();
    ?>
</body>
</html>

