<?php

@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'max_execution_time', '300' );

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
if ( ! file_exists( get_template_directory() . '/class-wp-bootstrap-navwalker.php' ) ) {
  // file does not exist... return an error.
  return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
  // file exists... require it.
  require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
function mytheme_localisation(){

    function mytheme_localised( $locale ) {
        if ( isset( $_GET['l'] ) ) {
            return sanitize_key( $_GET['l'] );
        }
        return $locale;
    }
    add_filter( 'locale', 'mytheme_localised' );
    load_theme_textdomain( 'mitema', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'mytheme_localisation' );
require get_template_directory().'/inc/opciones.php';
function rds_setup(){
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 500, 500 );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    #update_option( 'thumbnail_size_w', 253);
    #update_option( 'thumbnail_size_h', 164);
    #add_theme_support('html5',array('search-form'));
  }
  add_action( 'after_setup_theme', 'rds_setup' );
  function rds_style(){
		wp_register_style('normalize',get_template_directory_uri().'/css/normalize.css',array(),'5.0.0');
		wp_register_style('style',get_template_directory_uri().'/style.css',array('normalize'),'1.0');
    wp_enqueue_style('bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css");
		wp_enqueue_style('style');
    wp_enqueue_script("jquery");
    wp_enqueue_script("https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js");
    wp_enqueue_script("bootstrappopper","https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js");
    wp_enqueue_script("bootstrapjs","https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js");
	}
add_action('wp_enqueue_scripts','rds_style');
function rds_menus(){
	    register_nav_menus( array(
	      'header-menu' => __('Header Menu','rds'),
        'sub-menu' => __('Sub Menu','rds'),
) );
}
add_action( 'init','rds_menus' );
add_filter('wp_nav_menu_items','search_box_function', 10, 2);
function search_box_function( $nav, $args ) {
    if( $args->theme_location == 'header-menu' ){
      $imagen = get_template_directory_uri();
      $hu=home_url('/');
      $cl = pll_current_language();
      if ($cl=="es") {
        $ts="Buscar...";
      }
      else{
        $ts="Search...";
      }
 

      return $nav."<li class='menu-header-search menu-item menu-item-type-post_type menu-item-object-page'>
          <div id='myglass'>
            <a href='#' id='glass' class='s-close'><img src='$imagen/images/lupa.svg'></a>
          </div>
          <div class='contain-search' id='mysearch' style='display:none'>
            <form action='$hu' id='searchform' method='get'><input type='text' name='s' id='searching' placeholder='$ts'>
               <input type='submit' value='OK' class='submit_buttons_yellow'>
            </form>
      </div>
          
      </li>

      ";
        }
    return $nav;
}
add_action('wp_enqueue_scripts','rds');
function rds(){
  	wp_register_style('google_fonts','https://fonts.googleapis.com/css?family=Raleway:300,400,500,500i,700,700i,900,900i','1.0.0');
  	wp_register_script('scripts',get_template_directory_uri().'/js/scripts.js',array(),'1.0.0',true);
  	 wp_enqueue_script('scripts');
  }
add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );
function wpse156165_menu_add_class( $atts, $item, $args ) {
	    $class = 'nav-link'; // or something based on $item
	    $atts['class'] = $class;
	    return $atts;
	}
function rds_custom_logo(){
  	$logo =array(
  		'height' => 500,
  		'width' => 500
  	);
  	add_theme_support('custom-logo');
  }
add_action( 'after_setup_theme', 'rds_custom_logo');
add_filter( 'get_custom_logo', 'change_logo_class' );
function change_logo_class( $html ) {
	$html = str_replace( 'custom-logo', 'navbar-brand', $html );
    #$html = str_replace( 'custom-logo-link', 'your-custom-class', $html );
	return $html;
}
function my_secondary_menu_classes( $classes, $item, $args ) {
    // Only affect the menu placed in the 'secondary' wp_nav_bar() theme location
    if ( 'responsive-menu' === $args->theme_location ) {
        // Make these items 3-columns wide in Bootstrap
        $classes[] = 'page_item';
    }

    return $classes;
}
add_filter( 'nav_menu_css_class', 'my_secondary_menu_classes', 10, 3 ); 
add_action( 'init', 'rds_noticias' );
function rds_noticias() {
  $labels = array(
    'name'               => _x( 'Noticias', 'rds' ),
    'singular_name'      => _x( 'Noticia', 'post type singular name', 'rds' ),
    'menu_name'          => _x( 'Noticia', 'admin menu', 'rds' ),
    'name_admin_bar'     => _x( 'Noticia', 'add new on admin bar', 'rds' ),
    'add_new'            => _x( 'Add New', 'book', 'rds' ),
    'add_new_item'       => __( 'Agregar Noticia', 'rds' ),
    'new_item'           => __( 'Nueva noticia', 'rds' ),
    'edit_item'          => __( 'Editar Noticia', 'rds' ),
    'view_item'          => __( 'Ver Noticias', 'rds' ),
    'all_items'          => __( 'Todas las Noticias', 'rds' ),
    'search_items'       => __( 'Buscar noticia', 'rds' ),
    'parent_item_colon'  => __( 'Parent Noticia:', 'rds' ),
    'not_found'          => __( 'No Noticias| found.', 'rds' ),
    'not_found_in_trash' => __( 'No Noticia found in Trash.', 'rds' )
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'rds' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'noticias' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 6,
    'menu_icon'           => 'dashicons-megaphone',
    'supports'           => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'          => array( 'category','post_tag' ),
  );

  register_post_type( 'noticias', $args );
}

add_action( 'init', 'rds_pastorales' );
function rds_pastorales() {
  $labels = array(
    'name'               => _x( 'Pastoral', 'rds' ),
    'singular_name'      => _x( 'Pastoral', 'post type singular name', 'rds' ),
    'menu_name'          => _x( 'Pastoral', 'admin menu', 'rds' ),
    'name_admin_bar'     => _x( 'Pastoral', 'add new on admin bar', 'rds' ),
    'add_new'            => _x( 'Add New', 'book', 'rds' ),
    'add_new_item'       => __( 'Agregar Pastoral', 'rds' ),
    'new_item'           => __( 'Nueva Pastoral', 'rds' ),
    'edit_item'          => __( 'Editar Pastoral', 'rds' ),
    'view_item'          => __( 'Ver Pastoral', 'rds' ),
    'all_items'          => __( 'Todas las Pastoral', 'rds' ),
    'search_items'       => __( 'Buscar Pastoral', 'rds' ),
    'parent_item_colon'  => __( 'Parent Pastoral:', 'rds' ),
    'not_found'          => __( 'No Pastoral| found.', 'rds' ),
    'not_found_in_trash' => __( 'No Pastoral found in Trash.', 'rds' )
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'rds' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'pastorales' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 6,
    'menu_icon'           => 'dashicons-universal-access',
    'supports'           => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'          => array( 'category' ),
  );
  register_post_type( 'pastorales', $args );
}
add_action( 'init', 'rds_areas' );
function rds_areas() {
  $labels = array(
    'name'               => _x( 'Areas', 'rds' ),
    'singular_name'      => _x( 'Areas', 'post type singular name', 'rds' ),
    'menu_name'          => _x( 'Areas', 'admin menu', 'rds' ),
    'name_admin_bar'     => _x( 'Areas', 'add new on admin bar', 'rds' ),
    'add_new'            => _x( 'Add New', 'book', 'rds' ),
    'add_new_item'       => __( 'Agregar Area', 'rds' ),
    'new_item'           => __( 'Nueva area', 'rds' ),
    'edit_item'          => __( 'Editar area', 'rds' ),
    'view_item'          => __( 'Ver area', 'rds' ),
    'all_items'          => __( 'Todas las area', 'rds' ),
    'search_items'       => __( 'Buscar area', 'rds' ),
    'parent_item_colon'  => __( 'Parent area:', 'rds' ),
    'not_found'          => __( 'No Areas| found.', 'rds' ),
    'not_found_in_trash' => __( 'No Areas found in Trash.', 'rds' )
  );
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'rds' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'areas' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 6,
    'menu_icon'           => 'dashicons-welcome-learn-more',
    'supports'           => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'          => array( 'category' ),
  );

  register_post_type( 'areas', $args );
}
add_action( 'init', 'rds_notasadmisiones' );

// Register Custom Post Type
function rds_eventos() {

	$labels = array(
		'name'                  => _x( 'Eventos', 'rds'),
		'singular_name'         => _x( 'Evento', 'Post Type Singular Name', 'rds' ),
		'menu_name'             => __( 'Eventos', 'admin menu', 'rds' ),
		'name_admin_bar'        => __( 'Eventos', 'add new on admin bar', 'rds'),
		'archives'              => __( 'Item Archives', 'rds' ),
		'attributes'            => __( 'Item Attributes', 'rds' ),
		'parent_item_colon'     => __( 'Parent Item:', 'rds' ),
		'all_items'             => __( 'Todos los eventos', 'rds' ),
		'add_new_item'          => __( 'Agregar nuevo evento', 'rds' ),
		'add_new'               => __( 'Agregar nuevo evento', 'rds' ),
		'new_item'              => __( 'Nuevo Evento', 'rds' ),
		'edit_item'             => __( 'Editar Evento', 'rds' ),
		'update_item'           => __( 'Actualizar Evento', 'rds' ),
		'view_item'             => __( 'Ver Evento', 'rds' ),
		'view_items'            => __( 'Ver Eventos', 'rds' ),
		'search_items'          => __( 'Buscar Evento', 'rds' ),
		'not_found'             => __( 'Not found', 'rds' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rds' ),
		'featured_image'        => __( 'Featured Image', 'rds' ),
		'set_featured_image'    => __( 'Set featured image', 'rds' ),
		'remove_featured_image' => __( 'Remove featured image', 'rds' ),
		'use_featured_image'    => __( 'Use as featured image', 'rds' ),
		'insert_into_item'      => __( 'Insert into item', 'rds' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'rds' ),
		'items_list'            => __( 'Items list', 'rds' ),
		'items_list_navigation' => __( 'Items list navigation', 'rds' ),
		'filter_items_list'     => __( 'Filter items list', 'rds' ),
	);
	$args = array(
		'label'                 => __( 'evento', 'rds' ),
		'description'           => __( 'Evento description', 'rds' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'menu_icon'           => 'dashicons-welcome-widgets-menus',
		'capability_type'       => 'page',
	);
	register_post_type( 'evento', $args );

}
add_action( 'init', 'rds_eventos', 0 );

function rds_boton_cronograma_eventos() {

	$labels = array(
		'name'                  => _x( 'Botones cronograma', 'rds'),
		'singular_name'         => _x( 'Boton cronograma', 'Post Type Singular Name', 'rds' ),
		'menu_name'             => __( 'Boton cronograma ', 'admin menu', 'rds' ),
		'name_admin_bar'        => __( 'Boton cronograma', 'add new on admin bar', 'rds'),
		'archives'              => __( 'Item Archives', 'rds' ),
		'attributes'            => __( 'Item Attributes', 'rds' ),
		'parent_item_colon'     => __( 'Parent Item:', 'rds' ),
		'all_items'             => __( 'Boton cronograma', 'rds' ),
		'add_new_item'          => __( 'Agregar boton', 'rds' ),
		'add_new'               => __( 'Agregar boton', 'rds' ),
		'new_item'              => __( 'Nuevo Boton', 'rds' ),
		'edit_item'             => __( 'Editar Boton', 'rds' ),
		'update_item'           => __( 'Actualizar Boton', 'rds' ),
		'view_item'             => __( 'Ver Boton', 'rds' ),
		'view_items'            => __( 'Ver Boton', 'rds' ),
		'not_found'             => __( 'Not found', 'rds' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rds' ),
		'featured_image'        => __( 'Featured Image', 'rds' ),
		'set_featured_image'    => __( 'Set featured image', 'rds' ),
		'remove_featured_image' => __( 'Remove featured image', 'rds' ),
		'use_featured_image'    => __( 'Use as featured image', 'rds' ),
		'insert_into_item'      => __( 'Insert into item', 'rds' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'rds' ),
		'items_list'            => __( 'Items list', 'rds' ),
		'items_list_navigation' => __( 'Items list navigation', 'rds' ),
		'filter_items_list'     => __( 'Filter items list', 'rds' ),
	);
	$args = array(
		'label'                 => __( 'boton', 'rds' ),
		'description'           => __( 'boton description', 'rds' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'menu_icon'           => 'dashicons-calendar-alt',
		'capability_type'       => 'page',
	);
	register_post_type( 'boton_cronograma', $args );

}
add_action( 'init', 'rds_boton_cronograma_eventos', 0 );
function rds_notasadmisiones() {
  $labels = array(
    'name'               => _x( 'Notas Admisiones', 'rds' ),
    'singular_name'      => _x( 'Nota Admision', 'post type singular name', 'rds' ),
    'menu_name'          => _x( 'Notas Admisiones', 'admin menu', 'rds' ),
    'name_admin_bar'     => _x( 'N.Admisio', 'add new on admin bar', 'rds' ),
    'add_new'            => _x( 'Add New', 'book', 'rds' ),
    'add_new_item'       => __( 'Agregar nota admision', 'rds' ),
    'new_item'           => __( 'Nueva nota admision', 'rds' ),
    'edit_item'          => __( 'Editar nota admision', 'rds' ),
    'view_item'          => __( 'Ver nota admision', 'rds' ),
    'all_items'          => __( 'Todas las notas admision', 'rds' ),
    'search_items'       => __( 'Buscar nota admision', 'rds' ),
    'parent_item_colon'  => __( 'Parent notaadmision:', 'rds' ),
    'not_found'          => __( 'No admision| found.', 'rds' ),
    'not_found_in_trash' => __( 'No admision found in Trash.', 'rds' )
  );
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'rds' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'notas-admisiones' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 10,
    'menu_icon'           => 'dashicons-clipboard',
    'supports'           => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'          => array( 'category' ),
  );

  register_post_type( 'notasadmisiones', $args );
}
add_action( 'init', 'rds_publicacion' );
function rds_publicacion() {
  $labels = array(
    'name'               => _x( 'Publicacion', 'rds' ),
    'singular_name'      => _x( 'Publicacion', 'post type singular name', 'rds' ),
    'menu_name'          => _x( 'Publicaciones', 'admin menu', 'rds' ),
    'name_admin_bar'     => _x( 'Publicacion', 'add new on admin bar', 'rds' ),
    'add_new'            => _x( 'Add New', 'book', 'rds' ),
    'add_new_item'       => __( 'Agregar publicacion', 'rds' ),
    'new_item'           => __( 'Nueva Publicacion', 'rds' ),
    'edit_item'          => __( 'Editar Publicacion', 'rds' ),
    'view_item'          => __( 'Ver nota publicacion', 'rds' ),
    'all_items'          => __( 'Todas las publicacione', 'rds' ),
    'search_items'       => __( 'Buscar publicacion', 'rds' ),
    'parent_item_colon'  => __( 'Parent notaadmision:', 'rds' ),
    'not_found'          => __( 'No publicacion| found.', 'rds' ),
    'not_found_in_trash' => __( 'No publicacion found in Trash.', 'rds' )
  );
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'rds' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'publicacion' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 10,
    'menu_icon'           => 'dashicons-testimonial',
    'supports'           => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'          => array( 'category' ),
  );

  register_post_type( 'publicacion', $args );
}
add_action( 'init', 'rds_circular' );
function rds_circular() {
  $labels = array(
    'name'               => _x( 'circular', 'rds' ),
    'singular_name'      => _x( 'circular', 'post type singular name', 'rds' ),
    'menu_name'          => _x( 'Circulares', 'admin menu', 'rds' ),
    'name_admin_bar'     => _x( 'circular', 'add new on admin bar', 'rds' ),
    'add_new'            => _x( 'Add New', 'book', 'rds' ),
    'add_new_item'       => __( 'Agregar circular', 'rds' ),
    'new_item'           => __( 'Nueva circular', 'rds' ),
    'edit_item'          => __( 'Editar circular', 'rds' ),
    'view_item'          => __( 'Ver nota circular', 'rds' ),
    'all_items'          => __( 'Todas las circulares', 'rds' ),
    'search_items'       => __( 'Buscar circular', 'rds' ),
    'parent_item_colon'  => __( 'Parent circular:', 'rds' ),
    'not_found'          => __( 'No circular| found.', 'rds' ),
    'not_found_in_trash' => __( 'No circular found in Trash.', 'rds' )
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'rds' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'circular' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 11,
    'menu_icon'           => 'dashicons-media-text',
    'supports'           => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'          => array( 'category' ),
  );
  register_post_type( 'circular', $args );
}
function my_wpcf7_form_elements($html) {
  $text = 'Seleccione Asunto...';
  $html = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', $html);
  return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');
function my_excerpt_length($length) {
  return 40;
}
add_filter('excerpt_length', 'my_excerpt_length');
function aj_modify_posts_per_page( $query ) {
  if ( $query->is_search() ) {
  $query->set( 'posts_per_page', '4' );
  }
}
add_action( 'pre_get_posts', 'aj_modify_posts_per_page' );
add_action('init', function() {
  pll_register_string('Label noticias', 'noticias','index');
  pll_register_string('Label cronogram', 'cronograma','index');
  pll_register_string('Label leer más', 'leer_mas','All page');
  pll_register_string('Label más eventos', 'mas_eventos','All page');
  pll_register_string('Label descargar', 'descargar','All page');
  pll_register_string('Label extracurriculares', 'extracurriculares','index');
  pll_register_string('Label circulares', 'circulares','index');
  pll_register_string('Label intercambios', 'intercambios','index');
  pll_register_string('Label servicios', 'servicios','menus');
  pll_register_string('Title integrantes', 'integrantes_area','Single Areas');
  pll_register_string('Title description', 'descripcion_area','Single Areas');
  pll_register_string('Title formulario inscripcion', 'f_inscripcion','Admisiones');
  pll_register_string('Title pasos', 'step','Admisiones');
  pll_register_string('Title etapa', 'stage','Convenios');
  pll_register_string('Title etapa uno', 'etapa_uno','Convenios');
  pll_register_string('Title etapa dos', 'etapa_dos','Convenios');
  pll_register_string('Title etapa tres', 'etapa_tres','Convenios');
  pll_register_string('Title convenio jardines', 'convenio_jardines','Convenios');
  pll_register_string('Title in convenios Univeridades', 'universidades_sub','Convenios');
  pll_register_string('Title futsal', 'futsal','Escuelas');
  pll_register_string('Title ultimate', 'ultimate','Escuelas');
  pll_register_string('Title gimnasia', 'gimnasia','Escuelas');
  pll_register_string('Title atletismo', 'atletismo','Escuelas');
  pll_register_string('Title voleibol', 'voleibol','Escuelas');
  pll_register_string('Title futbol', 'futbol','Escuelas');
  pll_register_string('Title baloncesto', 'balonceso','Escuelas');
  pll_register_string('Title astronomia', 'astronomia','Escuelas');
  pll_register_string('Title porras', 'porras','Escuelas');
  pll_register_string('Title danzas', 'danzas','Escuelas');
  pll_register_string('Title matematicas', 'matematicas','Escuelas');
  pll_register_string('Title musica', 'musica','Escuelas');
  pll_register_string('Title zumba', 'zumba','Escuelas');
  pll_register_string('Title ingles', 'ingles','Escuelas');
  pll_register_string('Title dibujo manga', 'dibujo_manga','Escuelas');
  pll_register_string('Title arts lab', 'arts_lab','Escuelas');
  pll_register_string('Title pintura', 'pintura','Escuelas');
  pll_register_string('Title taekwondo', 'taekwondo','Escuelas');
  pll_register_string('Title ballet', 'ballet','Escuelas');


  pll_register_string('Title artes', 'title_artes','Artes');
  pll_register_string('Text artes', 'text_artes','Artes');
  pll_register_string('Boton artes', 'boton_artes','Artes');
  pll_register_string('Label actualidad', 'actualidad','index');		
  	

});
function pagination_nocitias( $custom_query ) {

    $total_pages = $custom_query->max_num_pages;
    $big = 999999999; // need an unlikely integer
    $b="<img src='".get_template_directory_uri()."/images/arrow_l_b.svg'>";
    $n="<img src='".get_template_directory_uri()."/images/arrow_r_b.svg'>";
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
  
            
            'prev_text'          => __($b),
            'next_text'          => __($n),
        ));
    }
}


//bloqueo de actualizaciones
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );


?>