<?php 
	function rds_ajustes(){
		add_menu_page('rds','Ajustes Plantilla','administrator','rds_ajustes','rds_opciones','',20);
		// opciones de tema
		

		add_action( 'admin_init', 'rds_registrar_opciones' );
	}

	add_action('admin_menu','rds_ajustes');

	function rds_registrar_opciones(){

		register_setting( 'rds_opciones_grupo', 'rds_eslogan' );
		register_setting( 'rds_opciones_grupo', 'rds_eslogan_en' );
		register_setting( 'rds_opciones_grupo', 'rds_video' );
		register_setting( 'rds_opciones_grupo', 'rds_video_en' );
		
		register_setting( 'rds_opciones_grupo', 'rds_facebook' );
		register_setting( 'rds_opciones_grupo', 'rds_tweeter' );
		register_setting( 'rds_opciones_grupo', 'rds_instagram' );
		register_setting( 'rds_opciones_grupo', 'rds_youtube' );

		register_setting( 'rds_opciones_grupo', 'artes_imagen' );
		register_setting( 'rds_opciones_grupo', 'artes_link' );
	}

	function rds_opciones(){
		?>
			<div class="wrap">
				<h1>Ajustes</h1>

				<form action="options.php" method="post">
					<?php settings_fields( 'rds_opciones_grupo' ); ?>
					<?php do_settings_sections( 'rds_opciones_grupo' ); ?>
					<table>
						<tr valign="top">
							<th scope="row">Eslogan</th>
							<td> <input type="text" name="rds_eslogan" value="<?php echo esc_attr(get_option('rds_eslogan')) ?>"></td>
						</tr>
						<tr valign="top">
							<th scope="row">Eslogan EN</th>
							<td> <input type="text" name="rds_eslogan_en" value="<?php echo esc_attr(get_option('rds_eslogan_en')) ?>"></td>
						</tr>
						<tr>
							<th scope="row">Video</th>
							<td> <input type="text" name="rds_video" value="<?php echo esc_attr(get_option('rds_video')) ?>"></td>
						</tr>

						<tr>
							<th scope="row">Video EN</th>
							<td> <input type="text" name="rds_video_en" value="<?php echo esc_attr(get_option('rds_video_en')) ?>"></td>
						</tr>
						
						<tr>
							<th scope="row">Facebook Link</th>
							<td> <input type="text" name="rds_facebook" value="<?php echo esc_attr(get_option('rds_facebook')) ?>"></td>
						</tr>
						<tr>
							<th scope="row">Tweeter Link</th>
							<td> <input type="text" name="rds_tweeter" value="<?php echo esc_attr(get_option('rds_tweeter')) ?>"></td>
						</tr>
						<tr>
							<th scope="row">Instagram Link</th>
							<td> <input type="text" name="rds_instagram" value="<?php echo esc_attr(get_option('rds_instagram')) ?>"></td>
						</tr>
						<tr>
							<th scope="row">Youtube Link</th>
							<td> <input type="text" name="rds_youtube" value="<?php echo esc_attr(get_option('rds_youtube')) ?>"></td>
						</tr>
						<tr>
							<th scope="row">Artes Imagen</th>
							<td> <input type="text" name="artes_imagen" value="<?php echo esc_attr(get_option('artes_imagen')) ?>"></td>
						</tr>
						<tr>
							<th scope="row">Artes Link</th>
							<td> <input type="text" name="artes_link" value="<?php echo esc_attr(get_option('artes_link')) ?>"></td>
						</tr>
					</table>
					<?php submit_button() ?>
				</form>
			</div>
		<?php

	}
		
?>