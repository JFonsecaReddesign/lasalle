<?php get_header(); ?>


<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue ">
	 	<div class="text-on-banner admisiones-header">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 			
	 			<p><?php the_content(); ?></p>
	 			
                <?php endwhile; endif; ?>
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>

<div class="container container-circular">
	<div class="row">
		
		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		    $args = array(
		    'post_type' => 'circular',
		    'posts_per_page' => 6,
		    'paged' => $paged,
			
			);
		$loop = new WP_Query( $args );
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-lg-6  col-12 conten-circulares" data-aos="zoom-out-right">
			<div class="row">
				<div class="col-3 circular-logo d-flex align-items-center">
					<img src="<?php echo get_template_directory_uri(); ?>/images/circular.svg" class="img-fluid">
				</div>
				<div class="col-9 circular-description">
					<span> <?php echo get_the_date(); ?> </span>
						<?php the_title('<h3>','</h3>') ?>
					<p>
						<?php the_content(); ?>
					</p>
					<a href="<?php echo get_field('archivo')['url']; ?>" class="submit_buttons" target="_blank"> <?php echo pll_e('descargar'); ?></a>
				</div>
			</div>
			
			
			
		</div>
		<?php endwhile; wp_reset_postdata(); ?>
		<div class="col-12 paginate pg-noticias justify-content-between">
						<?php pagination_nocitias( $loop ); ?>
				</div>
	</div>
</div>




<?php get_footer(); ?>