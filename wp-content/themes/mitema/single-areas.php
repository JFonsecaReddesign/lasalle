<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-12 single-areas">
			<?php the_title('<h1>','</h1>') ?>
	 	</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-5 col-12">
		<?php
			if (strpos($post->post_content,'[gallery') === false){ ?>
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid">
		<?php }
			else{
				?>
				<div id="siple_carousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    
  
 
				<?php
  				$gallery = get_post_gallery( get_the_ID(), false ) ;
			 	foreach ( $gallery['src'] AS $id => $src ) { 
			 		if ($id==0) {
			 			?>
			 			<div class="carousel-item active">
			 			<?php
					}
					else
					{
						?>
			 			<div class="carousel-item">
			 			<?php
					}
			 		?>
				      <img class="d-block w-100" src="<?php echo "$src" ?>" >
				    
				    </div>
				    <?php
	          	} ?>
</div>
	       
	         <a class="carousel-control-prev" href="#siple_carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#siple_carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
	        <?php

	        }
		   	
		?>
			
	 	</div>
	 	<div class="col-md-7 col-12 info_single_one">
	 	<h3><?php echo pll_e('integrantes_area'); ?></h3>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 			<div class="pre-scroll pre-single members">
	            	<?php the_field('integrantes'); ?>
	            </div>
            <?php endwhile; endif; ?>
	 	</div>
	 	<div class="col-12 info_single single-description">
	 	<h2><?php echo pll_e('descripcion_area'); ?></h2>
	 		<p>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); 
	 			
	 		$x= $posttext= get_the_content() ;	  
			$regex = '~<img [^\>]*\ />~';
			preg_match_all($regex, $x, $images);
			$posttext = preg_replace($regex, '', $posttext); 
			$posttext =  preg_replace("/\[[^)]+\]/","",$posttext);

			 echo the_content();
			?>
                   </p>
                <?php endwhile; endif; ?>
	 	</div>
	</div>
</div>




<?php get_footer(); ?>