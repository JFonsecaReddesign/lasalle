<?php get_header(); ?>
<div class="container search-container">
	<div class="row">
	<div class="col-12 single-areas search-title">
		<h1>BÚSQUEDA</h1>
		<p>
			Los siguientes son resultados de busqueda para la palabra usada "<?php echo get_search_query() ?>" :
		</p>
	</div>
		<?php
			if (have_posts()) {
				while(have_posts()):the_post(); ?>
					<div class="col-12 search-item">
					
					<?php the_title('<h2>','</h2>') ?>
					<p><?php the_excerpt(); ?></p>
					<a href="<?php the_permalink() ?>" class="btn">Leer Más</a>
					</div>
				<?php endwhile;
			}
			
		?>
	</div>
</div>
<div class="container containner-paginate">
	<div class="row">
		<div class="col-12 paginate">
			
			<?php 
			$b="<img src='".get_template_directory_uri()."/images/arrow_l_b.svg'>";
			$n="<img src='".get_template_directory_uri()."/images/arrow_r_b.svg'>";
			echo paginate_links( array(

  'prev_text' => $b,
  'next_text' => $n,

)); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>