<?php get_header(); ?>

<div class="contain-fluid banner-blue-text " >

	<div class="filter-blue ">
	 	<div class="text-on-banner admisiones-header">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 			<h2><?php the_field('sub-titulo'); ?></h2>
	 			<p><?php the_content(); ?></p>
	 			<a href="<?php echo get_field('adjunto')['url']; ?>" class="submit_buttons"><?php echo pll_e('f_inscripcion'); ?></a>
                <?php endwhile; endif; ?>
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>

<div class="container steps-container">
	<div class="row">
		<div class="col-md-6 d-none d-md-block">
		<div class="row justify-content-center tal">
		<?php while(have_posts()): the_post();?>

			<?php
				for ($i=1; $i<= 7; $i++) { ?>
				<div class="col-lg-3 col-md-6 title-steps">
				<a href="#" class="steps-click" value='<?php echo $i; ?>'>
					<img class="step-logo-b" src="<?php echo get_template_directory_uri(); ?>/images/step_<?php echo $i; ?>.svg" >
					<img class="step-logo-y" src="<?php echo get_template_directory_uri(); ?>/images/step_y_<?php echo $i; ?>.svg">
					<span><?php echo pll_e('step'); ?> <?php echo $i; ?></span>
					<h3><?php the_field('paso_'.$i.'_titulo'); ?></h3>
				</a>
			</div>
			<?php
				}
			?>
			
			
		
		</div>	
		</div>
		<div class="col-md-6">
		<div class="row steps-description">
			
				
			<?php
				for ($i=1; $i<= 7; $i++) { 
				if ($i==1) { ?>
				 	<div class="col-12  step-description" id="step-<?php echo $i ?>" data-aos="flip-right" >
				<?php } else{ ?>
					<div class="col-12 d-md-none step-description" id="step-<?php echo $i ?>" data-aos="flip-right">
				<?php }  ?>
					<div class="row">
					<div class="col-4 d-md-none">
						<img src="<?php echo get_template_directory_uri(); ?>/images/step_<?php echo $i; ?>.svg">
					</div>
					<div class="col-md-12 col-8 align-self-center">
						<span class="stepby" > <?php echo pll_e('step'); ?>  <?php echo $i; ?>:</span> <h3><?php the_field('paso_'.$i.'_titulo'); ?></h3>
					</div>
					<div class="col-12 step-full-description">
						<div class="pre-scroll-2" >
							<p> <?php the_field('paso_'.$i); ?> </p>
						</div>
					</div>
					</div>
				</div>
			<?php
				}
			?>
			
				
				</div>
			
		
		</div>
		<?php endwhile ?>
	</div>
	

</div>

<div class="container-fluid notes-admisiones">
<div class="row">
	<div class="col-12 ">
	<div id="admisionesCarousel" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">

	  	<?php
		    $args = array(
		    'post_type' => 'notasadmisiones',
		    'posts_per_page' => -1,
			
			);
		$query = new WP_Query($args);
		$c=1;
		while($query->have_posts()): $query->the_post();?>
			<?php 
			if ($c==1) {
				echo' <div class="carousel-item active">';
			}
			else{
				echo' <div class="carousel-item ">';
			} ?>

			<div class="col-12 notes-important">
		    	<div class="row">
		    		<div class="col-12 col-lg-6">
		    			<h2><?php the_title() ?></h2>
		    			<p><?php the_field('parrafo'); ?></p>
		    		</div>
		    		<div class="col-12 col-lg-6"> <?php the_content() ?> </div>
		    	</div>
		    </div>
			</div>

			<?php $c++; ?>
		
		<?php endwhile; wp_reset_postdata(); ?>

	    
	    
	    
	  </div>
	<a class="carousel-control-prev notes-arrows" href="#admisionesCarousel" role="button" data-slide="prev">
		<img src="<?php echo get_template_directory_uri(); ?>/images/arrow_l_b.svg" class="notes-arrows-img" >
	    
	  </a>
	  <a class="carousel-control-next notes-arrows" href="#admisionesCarousel" role="button" data-slide="next">
	    <img src="<?php echo get_template_directory_uri(); ?>/images/arrow_r_b.svg" class="notes-arrows-img" >
	  </a>
	</div>
	</div>
</div>
</div>
<?php get_footer(); ?>


