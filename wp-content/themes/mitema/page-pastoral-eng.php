<?php get_header(); ?>


<div class="contain-fluid banner-blue-text" >
	
	<div class="filter-blue">
	 	<div class="text-on-banner">
	 		<?php the_title('<h1>','</h1>') ?>
	 		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <p><?php the_content(); ?></p>
                <?php endwhile; endif; ?>
	 	</div>
	</div>
	<div class="blue-banner-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
	</div>
	
</div>



<?php $custom_query = new WP_Query( array( 'post_type' => 'pastorales' ) );
while($custom_query->have_posts()) : $custom_query->the_post(); ?> 
	<div class="container-fluid galery-independent" class="container-carouseles" style="display: none; width: 100%;" id="galery_independent_<?php echo get_the_ID(); ?>">
	<div class=" closer">
		<a href="#" class="closer-cross" value="<?php echo get_the_ID(); ?>">X</a> 
	</div>
	<div class="title">
		<?php the_title('<h2>','</h2>') ?>
	</div>
	<?php
		if ( $gallery = get_post_gallery( get_the_ID(), false ) ) { ?>
		 	<div id="Carousel_<?php echo get_the_ID(); ?>" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
	   		<?php foreach ( $gallery['src'] AS $id => $src ) { 

	          	if ($id==0) {
		   	?>
		   		<div class="carousel-item active pastoral-ima-item">
				    <img class="d-block img-fluid" src="<?php echo $src; ?>" alt="First slide" >
				</div>
		   	<?php
		   }
		   else{
		   	?>
		   		<div class="carousel-item pastoral-ima-item">
			      <img  class="d-block img-fluid" src="<?php echo $src; ?>" alt="Second slide" style="width: 100%;">
			    </div>
		   	<?php
		   	
		   }

	        } ?>
	        </div>
	        <a class="carousel-control-prev" href="#Carousel_<?php echo get_the_ID(); ?>" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#Carousel_<?php echo get_the_ID(); ?>" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
	        </div>
	    <?php } ?>
		
		<p>
			<?php
			  $x= $posttext = $post->post_content;	  
			  $regex = '~<img [^\>]*\ />~';
			  preg_match_all($regex, $x, $images);
			  $posttext = preg_replace($regex, '', $posttext); 
			  $posttext =  preg_replace("/\[[^)]+\]/","",$posttext);
			 echo $posttext; 
			?>
		</p>
		
	 
	</div>
<?php endwhile; ?>


<div class="container-fluid pastoral-container">
	<div class="row row-stuck">
		
		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'accompaniment-pastoral',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-5 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>

		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'educational-pastoral',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-4 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>

		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'organic-pastoral',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-3 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>

		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'family-pastoral',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-6 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>
				
		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'liturgical-sacramental',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-6 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>

		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'formation-pastoral',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-3 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>

		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'children-and-youth-pastoral',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-5 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>

		<?php
		    $args = array(
		    'post_type' => 'pastorales',
			'name' => 'social-pastoral',
			);
		$query = new WP_Query($args);
		while($query->have_posts()): $query->the_post();?>
		<div class="col-md-4 col-sm-12 pastoral-item" id="pastoral-item_<?php echo get_the_ID(); ?>" data-aos="zoom-in-up">
			<a href="#" value="<?php echo get_the_ID(); ?>" class="pastoral-function" >
			<div class="contain-pastorales">
				<div class="contain-pastorales-back" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					
				</div>
	    		<img src="<?php echo get_the_post_thumbnail_url(); ?>">
	    		<div class="hover-pastoral">
		   			<img src="<?php echo get_template_directory_uri(); ?>/images/hover_star.png" >
		   		</div>
				<div class="title-pastoral-thumbnail"><?php the_title('<h3>','</h3>') ?> </div>
	       	</div>
			</a>
		</div>
		<?php endwhile; wp_reset_postdata(); ?>

		
		
		
			
		
		
	</div>

	<?php wp_reset_postdata(); // reset the query ?>
</div>






<?php get_footer(); ?>




