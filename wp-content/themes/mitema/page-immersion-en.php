<?php get_header(); ?>

<div id="carouselBig" class="carousel slide  carousel-index  " data-ride="carousel">
  <div class="carousel-inner">
  	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 			
        <?php $url=apply_filters('the_content', $content); ?>
    <?php endwhile; endif; ?>
  	<?php


  		
		$gallery = get_post_gallery( $url, false );
		$ids = explode( ",", $gallery['ids'] );

		foreach( $ids as $id => $item ) {

		   $link   = wp_get_attachment_url( $item );

		   if ($id==0) {
		   	?>
		   		<div class="carousel-item active">
				    <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="First slide">
				</div>
		   	<?php
		   }
		   else{
		   	?>
		   		<div class="carousel-item">
			      <img class="d-block w-100 img-banner" src="<?php echo $link ?>" alt="Second slide">
			    </div>
		   	<?php
		   	
		   }
		} 
	?>
  
  </div>
<a class="carousel-control-prev" href="#carouselBig" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselBig" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>
<div class="container ">
	<div class="col-12 escuelas contain-page">
		<?php the_title('<h1>','</h1>') ?>
	</div>
	
</div>
<div class="contain-fluid contain-page">
	<div class="row">
		<div class="col-12 no-slide">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<p><?php the_content(); ?></p>
			<?php endwhile; endif; ?>
			
		</div>
	</div>
</div>	
<div class="page-footer">
<?php get_footer(); ?>
</div>